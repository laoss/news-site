<?php
// echo "<pre>" ; print_r( $_POST ) ;
if( empty( $_POST ) ) {
	echo "No data" ;
	exit ;
}
$uid = intval( $_POST[ 'user_id'] ) ;
if( empty( $uid ) ) {
	echo "Invalid user ID" ;
	exit ;
}

@include_once "user.php" ;
if( ! class_exists( "User" ) ) {
	echo "User.php load error" ;
	exit ;
} 

try {
	$user = new User( ) ;
	$user->loadUserDataById( $uid ) ;
} catch( Exception $ex ) {
	echo $ex->getMessage( ) ;
	exit ;
}

$user->first_name = $_POST[ 'user_name1' ] ;
$user->last_name  = $_POST[ 'user_name2' ] ;
$user->surname 	  = $_POST[ 'user_name3' ] ;
$user->role 	  = $_POST[ 'role' ] ;
// $user->id         = $uid ;
// $user->avatar     = $_POST[ '' ] ;

try {
	$res = $user->update( ) ;
} catch( Exception $ex ) {
	echo $ex->getMessage( ) ;
	exit ;
}

if( $res === false ) {
	echo "Something gone wrong ..." ;
} else {
	echo "Update OK" ;
}
