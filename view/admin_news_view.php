<table>
<tr><th>ID</th><th>Погоджено</th><th></th><th></th></tr>
<?php foreach( $all_news as $n ) : ?>
<tr>
  <td><?= $n[ 'id' ] ?>
	<input 
		type="button" 
		onclick="delNews(<?= $n[ 'id' ] ?>)" 
		value="Drop" 
	/>
  </td>
  <th>
	<input type="checkbox" 
		class="active_field"
		value="<?= $n[ 'id' ] ?>"
		<?= ( $n[ 'is_active' ] != 0 ) ? "checked" : "" ?>" 
	/>
  </th>
  <td>
	<a href="/admin/newsedit/<?= $n[ 'id' ] ?>" style="cursor:crosshair">
	  <img style="height:120px;width:auto;"
		   src="/<?= $n[ 'Image_file' ] ?>" 
	   />
	</a>
      </td>
  <td><?= $n[ 'Title_RU'    ] ?></td>
  <td><?= $n[ 'Content_RU'  ] ?></td>
  <td><?= $n[ 'ctg' ] . '(' . $n[ 'Id_Category' ] . ')' ?></td>
  <td><?= $n[ 'DT_create'   ] ?></td>
</tr>
<?php endforeach ; ?>
</table>

<script>
 chkClick = (e)=>{
	var x = new XMLHttpRequest();
	if(x) {
		var url = "/API/news_set_active.php?id="+e.target.value+"&act="+((e.target.checked)?'1':'0');
		x.open("GET", url, true);
		x.onreadystatechange = function() {
			if(x.readyState==4){
				console.log(url, x.responseText);
				var res = JSON.parse(x.responseText);
				if(res.status < 0) alert(res.descr);
				//else{ alert("OK"); window.location=window.location}
			}
		}
		x.send(null);
	}
 }
 var chks = document.getElementsByClassName("active_field");
 for(c of chks) c.onclick = chkClick;
 
 delNews = (id)=>{
	var x = new XMLHttpRequest();
	if(x) {
		x.open("GET", "/API/remove_news.php?id="+id, true);
		x.onreadystatechange = function() {
			if(x.readyState==4){
				console.log(x.responseText);
				var res = JSON.parse(x.responseText);
				if(res.status < 0) alert(res.descr);
				else{ alert("OK"); window.location=window.location}
			}
		}
		x.send(null);
	}
 }
</script>